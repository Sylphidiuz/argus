﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Argus : Acteur
{
    public CharacterController cc;
    public float speed = 2f;
    public float jumpHeight = 1f;
    public float gravityScale = -9.81f;
    public Vector3 velocity;
    public float xRotation = 0f;
    public float rotationSensibility = 100f;
    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
        velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void Backward()
    {
        velocity += -transform.forward *speed ;
    }

    public override void Fire()
    {
        throw new System.NotImplementedException();
    }

    public override void Forward()
    {
       velocity += transform.forward * speed ;
    }

    public override void Jump()
    {
        Debug.Log(transform.up* Mathf.Sqrt(jumpHeight * -2f * gravityScale));
        if (cc.isGrounded)
           velocity.y =  Mathf.Sqrt(jumpHeight * -2f * gravityScale);
    }

    public override void Left()
    {
       velocity += -transform.right * speed ;
    }

    public override void Right()
    {
       velocity += transform.right * speed ;
    }

    public override void RotationH(float v)
    {
        transform.Rotate(Vector3.up * v);
    }

    public override void RotationV(float v)
    {
        xRotation -= v * rotationSensibility * Time.deltaTime;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        head.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
       
    }

    public override void EndInputs()
    {
        cc.Move(velocity*Time.deltaTime* rotationSensibility * Time.deltaTime);
    }

    public override void StartInputs()
    {
        velocity = transform.up * velocity.y;
        if (cc.isGrounded && velocity.y < 0)
            velocity.y = -2f;
        velocity.y += gravityScale * Time.deltaTime;
    }
}
