﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Acteur : MonoBehaviour
{
    //Point where the camera have to stick;
    public GameObject head;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    abstract public void Forward();

    abstract public void Left();

    abstract public void Right();

    abstract public void Backward();

    abstract public void Jump();

    abstract public void Fire();

    abstract public void RotationH(float v);

    abstract public void RotationV(float v);

    abstract public void EndInputs();

    abstract public void StartInputs();


}
