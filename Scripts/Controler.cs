﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controler : MonoBehaviour
{
    public Camera headCamera;
    public Camera monitorCamera;
    public List<Acteur> controlables = new List<Acteur>();
    private int control = 0;
    private int watch = 0;
    private Acteur acteur;
    private Acteur watcher;
    public InputChecks inputs = new InputChecks();
    // Start is called before the first frame update
    void Start()
    {
        if (controlables.Count != 0)
        {
            acteur = controlables[control];
            watcher = controlables[watch];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (controlables.Count != 0)
        {
            acteur.StartInputs();
            if (inputs.Forward())
            {
                acteur.Forward();
            }
            if (inputs.Left())
            {
                acteur.Left();
            }
            if (inputs.Right())
            {
                acteur.Right();
            }
            if (inputs.Backward())
            {
                acteur.Backward();
            }
            if (inputs.Jump())
            {
                acteur.Jump();
            }
            if (inputs.Fire())
            {
                acteur.Fire();
            }
            if (inputs.RotationH() != 0)
            {
                acteur.RotationH(inputs.RotationH());
            }
            if (inputs.RotationV() != 0)
            {
                acteur.RotationV(inputs.RotationV());
            }
            acteur.EndInputs();
            UpdateCams();
        }
    }

    private void UpdateCams()
    {
        headCamera.transform.position = acteur.head.transform.position;
        monitorCamera.transform.position = watcher.head.transform.position;
        headCamera.transform.rotation = acteur.head.transform.rotation;
        monitorCamera.transform.rotation = watcher.head.transform.rotation;
    }
}

public class InputChecks
{
    virtual public bool Forward()
    {
        return Input.GetKey(KeyCode.Z);
    }

    virtual public bool Left()
    {
        return Input.GetKey(KeyCode.Q);
    }

    virtual public bool Backward()
    {
        return Input.GetKey(KeyCode.S);
    }

    virtual public bool Fire()
    {
        return false;
    }

    virtual public bool Jump()
    {
        return Input.GetKey(KeyCode.Space);
    }

    virtual public bool Right()
    {
        return Input.GetKey(KeyCode.D);
    }

    virtual public float RotationH()
    {
        return Input.GetAxis("Mouse X");
    }

    virtual public float RotationV()
    {
        return Input.GetAxis("Mouse Y");
    }
}
